#!/bin/sh

# remote repository on bitbucket needs to be created beforehand
# assume that we call it newrepo

# this script is executed in a client in which the template repo has been cloned
# need to do git checkout -b template the first time to create a branch called template
# git clone git@bitbucket.org:nip1516/template.git
# git checkout -b template

#this scripts replicates the content of the template repo into newrepo
#excluding the setup scripts


#hereafter the command to create the repository with the API (but it does not work for repos in a project)
#curl --user login:pass https://api.bitbucket.org/1.0/repositories/ --data name=REPO_NAME --data is_private=true


if [ -z ${1} ]; then
echo "Usage: ${0} <repo-name>"
exit 1
fi

#TEMPLATE="https://ssalsano@bitbucket.org/nip1516/"
TEMPLATE="ssalsano@bitbucket.org:nip1617/"
COMMAND="git remote add " 
#echo "COMMAND : ${COMMAND}$1 ${TEMPLATE}${1}.git"

${COMMAND}$1 ${TEMPLATE}${1}.git
#git remote add newrepo https://ssalsano@bitbucket.org/nip1516/newrepo.git

git checkout template 
git push ${1} template:master
#echo "after push"

#git push newrepo template:master

git checkout -b ${1}
#git checkout -b newrepo

git rm nip-add-repo.sh
git rm 2017-nip-add-repo.sh
git commit --author="StefanoSalsano <stefano.salsano@uniroma2.it>" -m "removing nip-add-repo.sh"
git push ${1} ${1}:master
#git push newrepo newrepo:master

git checkout template
git branch -D ${1}
#git branch -D newrepo

